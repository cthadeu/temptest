package com.example.temptest.domain


data class User(val id: String,
                val title: String,
                val firstName: String,
                val lastName: String,
                val email: String,
                val dateOfBirth: String,
                val phone: String,
                val location: Location) {

        fun getFullLocation(): String {
            return "${location.street} - ${location.state} - ${location.city}"
        }
}

data class Location(val street: String, val city: String, val state: String, val country: String)

