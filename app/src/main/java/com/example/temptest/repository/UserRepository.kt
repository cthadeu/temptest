package com.example.temptest.repository

import com.example.temptest.domain.User
import com.example.temptest.infrastructure.Config
import com.example.temptest.infrastructure.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface UserClientApi {
    @GET("user")
    fun findAll(): Call<UserListResponse<List<User>>>

    @GET("user/{id}")
    fun findById(@Path("id") id: String): Call<User>
}


data class UserListResponse<T>(val data: T)


class UserRepository() {
    private val retrofitClient = RetrofitClient.getInstance(Config().DummyApiHost)
    private val endpoint = retrofitClient.create(UserClientApi::class.java)

    fun findAll(callback: UserCallback<UserListResponse<List<User>>>) {
        endpoint.findAll().enqueue(object : Callback<UserListResponse<List<User>>> {
            override fun onResponse(call: Call<UserListResponse<List<User>>>, response: Response<UserListResponse<List<User>>>) {
                if (response.isSuccessful)
                    callback.onSuccess(response.body())
            }
            override fun onFailure(call: Call<UserListResponse<List<User>>>, t: Throwable) {
                callback.onError("Error getting users")
            }
        })
    }

    fun findById(id: String, callback: UserCallback<User>) {
        endpoint.findById(id).enqueue(object: Callback<User> {
            override fun onFailure(call: Call<User>, t: Throwable) {
                callback.onError("Error getting user data")
            }

            override fun onResponse(
                call: Call<User>,
                response: Response<User>
            ) {
                if(response.isSuccessful)
                    callback.onSuccess(response.body())
            }

        })
    }

    interface UserCallback<T> {
        fun onSuccess(data: T?)
        fun onError(error: String?)
    }


    companion object {
        private var instance: UserRepository? = null
        fun getInstance() = instance
            ?: UserRepository()
                .also { instance = it }
    }

}

