package com.example.temptest.view

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.temptest.R
import com.example.temptest.viewmodel.UserViewModel
import com.example.temptest.databinding.ActivityMainBinding
import com.example.temptest.domain.User

class MainActivity : AppCompatActivity() {

    private lateinit var usersList : ListView
    private lateinit var userViewModel: UserViewModel
    private lateinit var userListItems: ArrayList<User>
    private lateinit var userListAdapter: UserListViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        userViewModel = ViewModelProvider(this).get(UserViewModel::class.java)

        val binding: ActivityMainBinding = DataBindingUtil.setContentView(
                this, R.layout.activity_main
        )


        usersList = findViewById<ListView>(R.id.usersList)
        userListItems = arrayListOf()

        binding.viewModel = userViewModel
        binding.lifecycleOwner = this

        userListAdapter = UserListViewAdapter(
            this,
            userListItems
        )
        usersList.adapter = userListAdapter

        usersList.setOnItemClickListener() { adapterView, view, position, id ->
            val user = adapterView.getItemAtPosition(position) as User
            val intent = Intent(this, UserDetailActivity::class.java)
            intent.putExtra("userId", user.id)
            startActivity(intent)
        }
        userViewModel.users.observe(this, Observer<List<User>> {
            bindUserDataToList(it);
        })

        userViewModel.errorMessage.observe(this, Observer {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })

        userViewModel.getUsers()


    }

    private fun bindUserDataToList(users: List<User>) {
        userListItems.addAll(users)
        userListAdapter.notifyDataSetChanged()
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}