package com.example.temptest.view

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.temptest.R
import com.example.temptest.databinding.ActivityUserDetailBinding
import com.example.temptest.viewmodel.UserViewModel

class UserDetailActivity : AppCompatActivity() {
    private lateinit var userViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        userViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        var binding = DataBindingUtil.setContentView<ActivityUserDetailBinding>(
            this, R.layout.activity_user_detail
        )

        binding.viewModel = userViewModel
        binding.lifecycleOwner = this

        userViewModel.errorMessage.observe(this, Observer {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })

    }

    override fun onStart() {
        super.onStart()

        val userId = intent.getStringExtra("userId")
        if (userId != null) {
            userViewModel.getUserById(userId)
        }

    }
}