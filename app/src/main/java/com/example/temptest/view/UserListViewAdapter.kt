package com.example.temptest.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.temptest.R
import com.example.temptest.domain.User

class UserListViewAdapter(private val context: Context, private val users: List<User>) : BaseAdapter() {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val layout = LayoutInflater.from(context).inflate(R.layout.rows, parent, false)
        val name = layout.findViewById<TextView>(R.id.idName)
        val email = layout.findViewById<TextView>(R.id.idEmail)
        name.text = users[position].firstName
        email.text = users[position].email

        return layout
    }

    override fun getItem(position: Int): Any {
        return users[position]
    }

    override fun getItemId(position: Int): Long {
        return users.indexOf(getItem(position)).toLong()
    }

    override fun getCount(): Int {
        return users.size;
    }
}