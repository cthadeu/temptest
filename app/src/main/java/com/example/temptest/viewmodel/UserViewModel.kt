package com.example.temptest.viewmodel

import androidx.lifecycle.MutableLiveData

import androidx.lifecycle.ViewModel
import com.example.temptest.repository.UserListResponse
import com.example.temptest.repository.UserRepository
import com.example.temptest.domain.User
import com.example.temptest.repository.UserRepository.*


class UserViewModel: ViewModel() {

    private val userRepository = UserRepository.getInstance()

    var users = MutableLiveData<List<User>>().apply { listOf<User>() }
    var errorMessage = MutableLiveData<String>().apply { "" }
    var loading = MutableLiveData<Boolean>().apply { false }
    var user = MutableLiveData<User?>().apply {  }


    fun getUsers() {
        loading.value = true
        userRepository.findAll(object :
            UserCallback<UserListResponse<List<User>>> {
            override fun onSuccess(data: UserListResponse<List<User>>?) {
                loading.value = false
                users.value = data?.data
            }
            override fun onError(error: String?) {
                loading.value = false
                errorMessage.value = error
            }
        })
    }

    fun getUserById(id: String) {
        loading.value = true
        userRepository.findById(id, object : UserCallback<User> {
            override fun onSuccess(data: User?) {
                loading.value = false
                user.value = data
            }
            override fun onError(error: String?) {
                loading.value = false
                errorMessage.value = error
            }
        })
    }
}